#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Point {
   pub x: f64,
   pub y: f64,
}

/// Triangulate a polygon, returns a vector of indicies.
pub fn triangulate(poly: &[Point]) -> Option<Vec<usize>> {
   let n = poly.len();
   if n < 3 { return None; }

   let mut verts: Vec<usize> = (0..n).collect();
   if cross(poly) < 0.0 { verts.reverse(); }

   let mut result = Vec::new();
   let mut count = verts.len() * 2;
   let mut u;
   let mut v = verts.len() - 1;
   let mut w;

   while verts.len() > 2 {
      if count == 0 { return None; }
      count -= 1;

      u = v;     if u >= verts.len() { u = 0; }
      v = u + 1; if v >= verts.len() { v = 0; }
      w = v + 1; if w >= verts.len() { w = 0; }

      if cross(&[poly[verts[u]], poly[verts[v]], poly[verts[w]]]) == 0.0 {
         verts.remove(v);
         continue;
      }

      if is_ear(poly, &verts, u, v, w) {
         result.extend_from_slice(&[verts[u], verts[v], verts[w]]);
         verts.remove(v);
         count = verts.len() * 2;
      }
   }

   Some(result)
}

/// Test if the lines a and b intersect.
pub fn intersects(a: (Point, Point), b: (Point, Point)) -> bool {
   let o1 = cross(&[a.0, a.1, b.0]);
   let o2 = cross(&[a.0, a.1, b.1]);
   let o3 = cross(&[b.0, b.1, a.0]);
   let o4 = cross(&[b.0, b.1, a.1]);

   if (o1 > 0.0) != (o2 > 0.0) && (o3 > 0.0) != (o4 > 0.0) { return true; }

   o1 == 0.0 && in_segment(a.0, b.0, a.1) ||
   o2 == 0.0 && in_segment(a.0, b.1, a.1) ||
   o3 == 0.0 && in_segment(b.0, a.0, b.1) ||
   o4 == 0.0 && in_segment(b.0, a.1, b.1)
}

/// Test if some three points in a polygon are an ear.
fn is_ear(poly: &[Point], verts: &[usize], u: usize, v: usize, w: usize) -> bool {
   let tri = [poly[verts[u]], poly[verts[v]], poly[verts[w]]];
   if cross(&tri) < 0.0 { return false; }

   !verts.iter()
      .enumerate()
      .filter(|&(i,_)| !(i == u || i == v || i == w))
      .map(|(_,&i)| &poly[i])
      .any(|p| point_in_triangle(tri[0], tri[1], tri[2], *p))
}

/// Test if a diagonal a1->b is locally inside a clockwise polygon
/// (a0, a1, a2, ... b)
pub fn locally_inside(a0: Point, a1: Point, a2: Point, b: Point) -> bool {

   //let a0 = if u > 0 { poly[verts[u-1]] } else { poly[verts[verts.len() - 1]] };
   //let a1 = poly[verts[u]];
   //let a2 = if u < verts.len() { poly[verts[u+1]] } else { poly[verts[0]] };
   //let b = poly[verts[b]];

   if cross(&[a0, b, a2]) < 0.0 {
      cross(&[a1, b, a2]) >= 0.0 && cross(&[a1, a0, b]) >= 0.0
   } else {
      cross(&[a1, b, a0]) < 0.0 || cross(&[a1, a2, b]) < 0.0
   }
}

/// For colinear points a, b, and c, test if b lies on a->c.
fn in_segment(a: Point, b: Point, c: Point) -> bool {
   b.x <= f64::max(a.x, c.x) && b.x >= f64::min(a.x, b.x) &&
   b.y <= f64::max(a.y, c.y) && b.y >= f64::min(a.y, b.y)
}

/// Compute the scalar cross product of a polygon -- positive when the polygon
/// is clockwise.
fn cross(poly: &[Point]) -> f64 {
   let mut p = &poly[poly.len() - 1];
   poly.iter().map(|q| {
      let area = p.x * q.y - q.x * p.y;
      p = q;
      area
   }).sum()
}

/// Check if a point lies within a convex triangle
fn point_in_triangle(a: Point, b: Point, c: Point, p: Point) -> bool {
   (c.x - p.x) * (a.y - p.y) - (a.x - p.x) * (c.y - p.y) >= 0.0 &&
   (a.x - p.x) * (b.y - p.y) - (b.x - p.x) * (a.y - p.y) >= 0.0 &&
   (b.x - p.x) * (c.y - p.y) - (c.x - p.x) * (b.y - p.y) >= 0.0
}

#[cfg(test)]
mod tests {
   #[test] fn cross() {
      assert_eq!(8.0, ::cross(&[
         ::Point { x: -1.0, y: -1.0 },
         ::Point { x: 1.0, y: -1.0 },
         ::Point { x: 1.0, y: 1.0 },
         ::Point { x: -1.0, y: 1.0 },
      ]));

      assert_eq!(-8.0, ::cross(&[
         ::Point { x: -1.0, y: -1.0 },
         ::Point { x: -1.0, y: 1.0 },
         ::Point { x: 1.0, y: 1.0 },
         ::Point { x: 1.0, y: -1.0 },
      ]));
   }

   #[test] fn inside_triangle() {
      let tests = &[
         (true, ::Point{ x: 1.0, y: 0.5 }),
         (true, ::Point{ x: 0.5, y: 0.4 }),
         (false, ::Point{ x: 0.5, y: 0.6 }),
         (false, ::Point{ x: 1.0, y: -0.1 }),
      ];

      for &(expected, point) in tests {
         assert_eq!(expected, ::point_in_triangle(
            ::Point { x: 0.0, y: 0.0 },
            ::Point { x: 2.0, y: 0.0 },
            ::Point { x: 1.0, y: 1.0 },
            point
         ));
      }
   }

   #[test] fn is_ear() {
      let poly = &[
         ::Point { x: 0.0, y: 0.0 },
         ::Point { x: -1.0, y: 1.0 },
         ::Point { x: 0.0, y: -1.0 },
         ::Point { x: 1.0, y: 1.0 },
      ];

      let verts = &[0, 1, 2, 3];

      assert_eq!(true, ::is_ear(poly, verts, 0, 1, 2));
      assert_eq!(false, ::is_ear(poly, verts, 1, 2, 3));
      assert_eq!(true, ::is_ear(poly, verts, 2, 3, 0));
      assert_eq!(false, ::is_ear(poly, verts, 3, 0, 1));
   }

   #[test] fn triangulate() {
      let poly = &[
         ::Point { x: 0.0, y: 0.0 },
         ::Point { x: -1.0, y: 1.0 },
         ::Point { x: 0.0, y: -1.0 },
         ::Point { x: 1.0, y: 1.0 },
      ];

      assert!(::triangulate(poly).is_some());
   }

   #[test] fn intersects() {
      assert!(::intersects(
            (::Point{ x: 0.0, y: 0.0 }, ::Point{ x: 0.0, y: 1.0 }),
            (::Point{ x: 0.0, y: 0.0 }, ::Point{ x: 0.0, y: 1.0 })));

      assert!(::intersects(
            (::Point{ x: 0.0, y: 0.0 }, ::Point{ x: 0.0, y: 2.0 }),
            (::Point{ x: 0.0, y: 0.0 }, ::Point{ x: 0.0, y: 1.0 })));

      assert!(::intersects(
            (::Point{ x: 0.0, y: 0.0 }, ::Point{ x: 1.0, y: 1.0 }),
            (::Point{ x: 0.0, y: 1.0 }, ::Point{ x: 1.0, y: 0.0 })));

      assert!(::intersects(
            (::Point{ x: 0.0, y: 0.0 }, ::Point{ x: 1.0, y: 1.0 }),
            (::Point{ x: 1.0, y: 0.0 }, ::Point{ x: 0.0, y: 1.0 })));

      assert!(!::intersects(
            (::Point{ x: 0.0, y: 0.0 }, ::Point{ x: 0.0, y: 1.0 }),
            (::Point{ x: 1.0, y: 0.0 }, ::Point{ x: 1.0, y: 1.0 })));
   }

   #[test] fn locally_inside() {
      assert!(::locally_inside(
            ::Point{ x: 1.0, y: -1.0 }, ::Point{ x: 0.0, y: 0.0 }, ::Point{ x: 1.0, y: 1.0 },
            ::Point{ x: 2.0, y: 0.0 }));

      assert!(::locally_inside(
            ::Point{ x: 1.0, y: 1.0 }, ::Point{ x: 0.0, y: 0.0 }, ::Point{ x: 1.0, y: -1.0 },
            ::Point{ x: -2.0, y: 0.0 }));

      assert!(!::locally_inside(
            ::Point{ x: 1.0, y: -1.0 }, ::Point{ x: 0.0, y: 0.0 }, ::Point{ x: 1.0, y: 1.0 },
            ::Point{ x: -2.0, y: 0.0 }));

      assert!(!::locally_inside(
            ::Point{ x: 1.0, y: 1.0 }, ::Point{ x: 0.0, y: 0.0 }, ::Point{ x: 1.0, y: -1.0 },
            ::Point{ x: 2.0, y: 0.0 }));
   }
}
